<?php


namespace App\Controller;

use \App;
use Core\Controller\Controller;

class AppController extends Controller{

    //protected $template = 'default';
    protected $template = 'theme';

    public function __construct(){
        $this->viewPath = ROOT . '/app/Views/';
    }

    protected function loadModel($model_name){
        return $this->$model_name = App::getInstance()->getTable($model_name);
    }

}