<?php


namespace App\Controller;

use \App;

class PostController extends AppController{

    public function __construct(){
        parent::__construct();
        $this->loadModel('Post');
        $this->loadModel('Category');
    }

    public function index(){
        $posts = $this->Post->last();
        $categories = $this->Category->all();
        $this->render('post/index', compact('posts', 'categories'));
    }

    public function category(){
        $category = $this->Category->find($_GET['id']);

        if($category === false){
            $this->error404();
        }

        $posts = $this->Post->lastByCategory($_GET['id']);
        $categories = $this->Category->all();
        App::setPageTitle($category->name);
        $this->render('post/category', compact('posts', 'categories', 'category'));
    }

    public function show(){
        $post = $this->Post->findWithCategory($_GET['id']);
        App::setPageTitle($post->title);
        $this->render('post/show', compact('post'));
    }

    public function all(){
        $posts = $this->Post->all();
        $categories = $this->Category->all();
        $this->render('post/all', compact('posts', 'categories'));
    }
}