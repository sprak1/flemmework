<?php

namespace App\Controller\Admin;

use App;
use Core\HTML\BootstrapForm;

class PostController extends AppController
{
    public function __construct(){
        parent::__construct();
        $this->loadModel('Post');
    }

    public function index(){
        $posts = $this->Post->all();
        App::setPageTitle('Post manager');
        $this->render('admin/post/index', compact('posts'));
    }

    public function add(){
        if(!empty($_POST)) {
            $query = $this->Post->create([
                'title' => $_POST['title'],
                'content' => $_POST['content'],
                'category_id' => $_POST['category_id']
            ]);

            if ($query) {
                return $this->index();
            }
        }
        $this->loadModel('Category');
        $categories = $this->Category->liste('id', 'name');
        $form = new BootstrapForm($_POST);
        App::setPageTitle('New');
        $this->render('admin/post/edit', compact('categories', 'form'));
    }

    public function edit(){
        if(!empty($_POST)){
            $query = $this->Post->update($_GET['id'], [
                'title' => $_POST['title'],
                'content' => $_POST['content'],
                'category_id' => $_POST['category_id']
            ]);
            if($query){
                return $this->index();
            }
        }
        $this->loadModel('Category');
        $post = $this->Post->find($_GET['id']);
        $categories = $this->Category->liste('id', 'name');
        $form = new BootstrapForm($post);
        App::setPageTitle($post->title . ' [edit]');
        $this->render('admin/post/edit', compact('categories', 'form'));
    }

    public function delete(){
        if(!empty($_POST)){
            $this->Post->delete($_POST['id']);
            return $this->index();
        }
    }
}