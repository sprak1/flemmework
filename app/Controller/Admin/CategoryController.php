<?php

namespace App\Controller\Admin;

use App;
use Core\HTML\BootstrapForm;

class CategoryController extends AppController
{
    public function __construct(){
        parent::__construct();
        $this->loadModel('Category');
    }

    public function index(){
        $categories = $this->Category->all();
        App::setPageTitle('Category manager');
        $this->render('admin/category/index', compact('categories'));
    }

    public function add(){
        if(!empty($_POST)){
            $this->Category->create([
                'name' => $_POST['name']
            ]);
            return $this->index();
        }
        $form = new BootstrapForm($_POST);
        App::setPageTitle('New');
        $this->render('admin/category/edit', compact('form'));
    }

    public function edit(){
        if(!empty($_POST)){
            $this->Category->update($_GET['id'], [
                'name' => $_POST['name']
            ]);
            return $this->index();
        }
        $categorie = $this->Category->find($_GET['id']);
        $form = new BootstrapForm($categorie);
        App::setPageTitle($categorie->name . ' [edit]');
        $this->render('admin/category/edit', compact('form'));
    }

    public function delete(){
        if(!empty($_POST)){
            $this->Category->delete($_POST['id']);
            return $this->index();
        }
    }
}