<?php


namespace App\Controller;


use Core\Auth\DbAuth;
use Core\HTML\BootstrapForm;
use \App;

class UserController extends AppController{
    public function login(){
        $error = false;
        $auth = new DbAuth(App::getInstance()->getDb());
        if(!empty($_POST)){
            if($auth->login($_POST['username'], $_POST['password'])){
                header('Location: index.php?p=admin.post.index');
            }else{
                $error = true;
            }
        }
        if(!$auth->is_logged()){
            $form = new BootstrapForm($_POST);
            App::setPageTitle('Login');
            $this->render('user/login', compact('form', 'error'));
        }else{
            $this->loadModel('Post');
            $posts = $this->Post->all();
            App::setPageTitle('Admin');
            $this->render('admin/post/index', compact('posts'));
        }
    }

}