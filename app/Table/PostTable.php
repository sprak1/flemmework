<?php
namespace App\Table;

use Core\Table\Table;

class PostTable extends Table
{

    /**
     * Get all posts
     * @return array
     */
    public function all(){
        return $this->query("
            SELECT post.id, post.title, post.content, post.created, post.updated, category.name as category
            FROM post
            LEFT JOIN category ON category_id = category.id
            ORDER BY post.created DESC
        ");
    }

    /**
     * Get last posts
     * @return array
     */
    public function last(){
        return $this->query("
            SELECT post.id, post.title, post.content, post.created, post.updated, category.name as category
            FROM post
            LEFT JOIN category ON category_id = category.id
            ORDER BY post.created DESC
            LIMIT 5
        ");
    }

    /**
     * Get one post by id
     * @param $id
     * @return array|mixed
     */
    public function findWithCategory($id){
        return $this->query("
            SELECT post.id, post.title, post.content, post.created, post.updated, category.name as category
            FROM post
            LEFT JOIN category ON category_id = category.id
            WHERE post.id = ?
        ", [$id], true);
    }

    /**
     * Get last posts by category
     * @param $category_id
     * @return array|mixed
     */
    public function lastByCategory($category_id){
        return $this->query("
            SELECT post.id, post.title, post.content, post.created, post.updated, category.name as category
            FROM post
            LEFT JOIN category ON category_id = category.id
            WHERE post.category_id = ?
            ORDER BY post.updated DESC
        ", [$category_id]);
    }
}
