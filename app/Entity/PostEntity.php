<?php


namespace App\Entity;

use Core\Entity\Entity;

class PostEntity extends Entity
{

    public function getUrl()
    {
        return '?p=post.show&id=' . $this->id;
    }

    public function getExcerp()
    {
        return '<p>' . substr($this->content, 0, 100) . '...</p>';
    }

}