<?php


namespace App\Entity;

use Core\Entity\Entity;

class CategoryEntity extends Entity
{

    public function getUrl()
    {
        return 'index.php?p=post.category&id=' . $this->id;
    }

    public function getExcerp()
    {
        $html = '<p>' . substr($this->contenu, 0, 100) . '...</p>';
        $html .= '<p><a href="' . $this->getUrl() . '">Voir la suite</a></p>';
        return $html;
    }

}