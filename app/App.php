<?php

use Core\Database\MysqlDatabase;
use Core\Config;

class App{

    private static $_instance;
    private  $db_instance;
    private static $page_title = "Flemmework";

    public static function getInstance(){
        if(is_null(self::$_instance)){
            self::$_instance = new App();
        }
        return self::$_instance;
    }

    public static function load(){
        session_start();
        require ROOT.'/app/Autoloader.php';
        App\Autoloader::register();
        require ROOT.'/core/Autoloader.php';
        Core\Autoloader::register();
    }

    public function getTable($name){
        $class_name = '\\App\\Table\\' . ucfirst($name) . 'Table';
        return new $class_name($this->getDb());
    }

    public function getDb(){
        $config = Config::getInstance(ROOT.DS.'config'.DS.'config.php');
        if(is_null($this->db_instance)){
            $this->db_instance =  new MysqlDatabase($config->get('db_name'), $config->get('db_user'), $config->get('db_pass'), $config->get('db_host'));
        }
        return $this->db_instance;
    }


    /**
     * @return string
     */
    public static function getPageTitle()
    {
        return self::$page_title;
    }

    /**
     * @param string $page_title
     */
    public static function setPageTitle($page_title)
    {
        self::$page_title .= ' | ' .$page_title;
    }
}