<main class="valign-wrapper">
    <div class="container valign">

        <!--  Tables Section-->
        <div id="login" class="row">
            <div class="col s12 m6 offset-m2 offset-l3">
                <?php if($error): ?>
                    <div class="">Authentication failed</div>
                <?php endif; ?>
            </div>
            <!-- <h1 class="thin">Login</h1> -->
            <div class="col s12 m8 l6 offset-m2 offset-l3 card-panel">
                <form method="post" class="login-form">
                    <div class="row form-header blue-grey white-text">
                        <div class="col s4">
                            <a href="./">
                                <img src="img/admin-logo-full.svg" alt="logo" class="logo responsive-img-height">
                            </a>
                        </div>
                        <div class="col s8 right-align">
                            <h4 class="light white-text">Login</h4>
                        </div>
                    </div>
                    <div class="form-body">
                        <?= $form->input('username', 'Username'); ?>
                        <?= $form->input('password', 'Password', ['type' => 'password']); ?>
                        <?= $form->submit(); ?>

                        <div class="row">
                            <div class="col s6">
                                <p class="margin medium-small"><a href="register.html">Register Now!</a></p>
                            </div>
                            <div class="col s6">
                                <p class="margin right-align medium-small"><a href="reset-password.html">Forgot password?</a></p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- container end -->
</main>
