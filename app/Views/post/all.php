<?php foreach ($posts as $post): ?>
    <li class="collection-item">
        <p><a href="<?= $post->url; ?>"><?= $post->title; ?></a><br>
            <span class="badge static neutral"><?= $post->category; ?></span>
            <?php $date_post = date_create($post->created); ?>
            <span class="secondary-content"><?= date_format($date_post, 'd/m/Y, H:i:s'); ?></span>
        </p>
    </li>
<?php endforeach; ?>