<?php $this_category = $category->name; ?>
<header>
    <!-- Navigation Bar on the top, for medium and small devices -->
    <div class="brand-logo hide-on-large-only blue-grey white-text"><a href="./"><img src="img/admin-logo-full.svg" alt="logo" class="logo responsive-img"></a></div>
    <div class="navbar-fixed hide-on-large-only">
        <nav>
            <div class="nav-wrapper">
                <ul class="right">
                    <li class="hide-on-small-only"><a href="#search-in-modal" class="modal-trigger"><i class="material-icons">search</i></a></li>
                    <li class="hide-on-small-only"><a href="account.html"><i class="material-icons">perm_identity</i></a></li>
                    <li class="hide-on-small-only"><a href="login.html" target="_blank"><i class="material-icons">exit_to_app</i></a></li>
                    <li class="toogle-side-nav"><a href="#" data-activates="slide-menu" class="button-collapse"><i class="material-icons">menu</i></a></li>
                </ul>
            </div>
        </nav>
    </div>
    <!-- Side Navigation - fixed for large (nice scroll with Simplebar plugin), slide/drag for medium and small devices -->
    <div id="slide-menu" class="side-nav fixed" data-simplebar-direction="vertical">
        <ul class="side-nav-main">
            <li class="logo hide-on-med-and-down blue-grey white-text"><a href="./"><img src="img/admin-logo-full.svg" alt="<>" class="logo responsive-img"></a></li>
            <li class="side-nav-inline hide-on-med-only">
                <a href="./" class="inline waves-effect"><i class="material-icons">home</i></a>
                <a href="?p=user.login" class="inline waves-effect"><i class="material-icons">face</i></a>
                <a href="#" class="inline waves-effect"><i class="material-icons">search</i></a>
            </li>
            <li>
                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <a class="collapsible-header waves-effect">
                            <i class="material-icons left">list</i>
                            <span>Categories</span>
                            <!--<span class="neutral badge">4</span>-->
                        </a>
                        <div class="collapsible-body">
                            <ul>
                                <?php foreach ($categories as $category): ?>
                                    <li><a href="<?= $category->url; ?>"><?= $category->name; ?> <!--<span class="neutral badge">4</span>--></a></li>
                                    <li class="divider"></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</header>
<main>
    <div class="container">
        <div id="posts" class="card hoverable">
            <ul class="collection with-header">
                <li class="collection-header primary-color">
                    <h5 class="light secondary-color-text">Category : <?= $this_category; ?> <a href="#!" class="secondary-content tooltipped" data-position="bottom" data-delay="50" data-tooltip="All" data-tooltip-id="90f1f475-dc69-6370-f78e-b062a67389db"><i class="material-icons secondary-color-text">arrow_forward</i></a></h5>
                </li>
                <?php foreach ($posts as $post): ?>
                    <li class="collection-item">
                        <p><a href="<?= $post->url; ?>"><?= $post->title; ?></a><br>
                            <span class="badge static neutral"><?= $this_category; ?></span>
                            <?php $date_post = date_create($post->created); ?>
                            <span class="secondary-content"><?= date_format($date_post, 'd/m/Y, H:i:s'); ?></span>
                        </p>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div><!-- ./div.container -->
</main>
