<!DOCTYPE html>
<html>
<head>
    <meta charset='UTF-8'>
    <meta name="robots" content="noindex">
    <meta name="description" content="Search your inside digital world">
    <meta name="keywords" content="Blog, IT, Dev">
    <meta name="author" content="Prak Sambeau">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= App::getInstance()->getPageTitle(); ?></title>
    <link rel='stylesheet' href='www/materialize/css/materialize.min.css'>
    <link rel="stylesheet" href="www/plugins/trumbowyg/dist/ui/trumbowyg.min.css">
    <link rel='stylesheet' href='www/css/style.css'>
    <link rel="stylesheet" href="www/plugins/highlight/styles/monokai-sublime.css">
</head>
<body>
<header>
    <nav class="top-nav teal">
        <div class="row">
            <div class="col s12 m4 l3">
                <div class="nav-wrapper"><a class=""><h4><> Flemmework</h4></a></div>
            </div>
        </div>
    </nav>
</header>
<main>
    <div class="row">
        <?= $content; ?>
    </div>
</main>
<script src="www//js/jquery.min.js"></script>
<script src="www//materialize/js/materialize.min.js"></script>
<script src="www//plugins/trumbowyg/dist/trumbowyg.min.js"></script>
<script src="www//plugins/trumbowyg/plugins/preformatted/trumbowyg.preformatted.js"></script>
<script type="text/javascript" src="www//plugins/trumbowyg/dist/langs/fr.min.js"></script>
<script>
    $('select').material_select();
    $('textarea').trumbowyg({
        lang: 'fr',
        btns: [
            ['viewHTML'],
            ['formatting'],
            ['preformatted'],
            'btnGrp-semantic',
            ['superscript', 'subscript'],
            ['link'],
            ['insertImage'],
            'btnGrp-justify',
            'btnGrp-lists',
            ['horizontalRule'],
            ['removeformat'],
            ['fullscreen']
        ],
        autogrow: true,
        resetCss: true
    });
</script>
<script src="www//plugins/highlight/highlight.pack.js"></script>
<script>hljs.initHighlightingOnLoad();</script>
</body>
</html>
