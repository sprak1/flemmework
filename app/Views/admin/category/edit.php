<header>
    <!-- Navigation Bar on the top, for medium and small devices -->
    <div class="brand-logo hide-on-large-only blue-grey white-text"><a href="./"><img src="img/admin-logo-full.svg" alt="logo" class="logo responsive-img"></a></div>
    <div class="navbar-fixed hide-on-large-only">
        <nav>
            <div class="nav-wrapper">
                <ul class="right">
                    <li class="hide-on-small-only"><a href="#search-in-modal" class="modal-trigger"><i class="material-icons">search</i></a></li>
                    <li class="hide-on-small-only"><a href="account.html"><i class="material-icons">perm_identity</i></a></li>
                    <li class="hide-on-small-only"><a href="login.html" target="_blank"><i class="material-icons">exit_to_app</i></a></li>
                    <li class="toogle-side-nav"><a href="#" data-activates="slide-menu" class="button-collapse"><i class="material-icons">menu</i></a></li>
                </ul>
            </div>
        </nav>
    </div>
    <!-- Side Navigation - fixed for large (nice scroll with Simplebar plugin), slide/drag for medium and small devices -->
    <div id="slide-menu" class="side-nav fixed" data-simplebar-direction="vertical">
        <ul class="side-nav-main">
            <li class="logo hide-on-med-and-down blue-grey white-text"><a href="./"><img src="img/admin-logo-full.svg" alt="<>" class="logo responsive-img"></a></li>
            <li class="side-nav-inline hide-on-med-only">
                <a href="./" class="inline waves-effect"><i class="material-icons">home</i></a>
                <a href="?p=user.login" class="inline waves-effect"><i class="material-icons">face</i></a>
                <a href="#" class="inline waves-effect"><i class="material-icons">search</i></a>
            </li>
            <li>
                <ul class="collapsible" data-collapsible="accordion">
                    <li><a href="?p=admin.post.index" class="collection-item active"><i class="fa fa-newspaper-o"></i> Post manager</a></li>
                    <li><a href="?p=admin.category.index" class="collection-item"><i class="fa fa-navicon"></i> Category manager</a></li>
                </ul>
            </li>
        </ul>
    </div>
</header>
<main>
    <div class="container">
        <form method="post">
            <form method="post">
                <?= $form->input('name', 'Category name'); ?>
                <?= $form->submit(); ?>
            </form>
        </form>
    </div><!-- ./div.container -->
</main>
