<?php
namespace Core\Table;

use Core\Database\Database;

class Table
{

    protected $table;
    protected $db;

    public function __construct(Database $db)
    {
        $this->db = $db;
        if(is_null($this->table)) {
            $classes = explode('\\', get_class($this));
            $class_name = end($classes);
            $this->table = strtolower(str_replace(('Table'), '', $class_name));
        }
    }

    public function all(){
        return $this->query('SELECT * FROM ' . $this->table);
    }

    public function query($statement, $attr = null, $one = false){
        if($attr){
            return $this->db->prepare(
                $statement,
                $attr,
                str_replace('Table', 'Entity', get_class($this)),
                $one
            );
        }else{
            return $this->db->query(
                $statement,
                str_replace('Table', 'Entity', get_class($this)),
                $one
            );
        }
    }

    public function find($id){
        return $this->query("SELECT * FROM {$this->table} WHERE id =?", [$id], true);
    }

    public function liste($key, $value){
        $records = $this->all();
        $return = [];
        foreach ($records as $k){
            $return[$k->$key] = $k->$value;
        }
        return $return;
    }

    public function update($id, $fields){
        $sql_parts = [];
        $attr = [];
        foreach ($fields as $k => $v){
            $sql_parts[] = "$k = ?";
            $attr[] = $v;
        }
        $attr[] = $id;
        $sql_part = implode(', ', $sql_parts);
        return $this->query("UPDATE {$this->table} SET $sql_part WHERE id = ?", $attr, true);
    }

    public function create($fields){
        $sql_parts = [];
        $attr = [];
        foreach ($fields as $k => $v){
            $sql_parts[] = "$k = ?";
            $attr[] = $v;
        }
        $sql_part = implode(', ', $sql_parts);
        return $this->query("INSERT INTO {$this->table} SET $sql_part", $attr, true);
    }

    public function delete($id){
        return $this->query("DELETE FROM {$this->table} WHERE id = ?", [$id], true);
    }

}