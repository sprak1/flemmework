<?php

namespace Core\Auth;
use Core\Database\Database;

class DbAuth {

    private $db;

    public function __construct(Database $db){
        $this->db = $db;
    }

    public function getUserId(){
        if($this->is_logged()){
            return $_SESSION['auth'];
        }
        return false;
    }
    public function login($username, $password){
        $user = $this->db->prepare('SELECT * FROM user WHERE username = ?', [$username], null, true);
        if($user){
            if($user->password === sha1($password)){
                $_SESSION['auth'] = $user->id;
                return true;
            }
        }
        return false;
    }

    public function is_logged(){
        return isset($_SESSION['auth']);
    }

}