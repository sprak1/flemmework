<?php

namespace Core\Database;
use \PDO;

class MysqlDatabase extends Database{

    private $db_name;
    private $db_user;
    private $db_pass;
    private $db_host;
    private $pdo;

    public function __construct($db_name, $db_user = 'root', $db_pass = 'root', $db_host = 'localhost')
    {
        $this->db_name = $db_name;
        $this->db_user = $db_user;
        $this->db_pass = $db_pass;
        $this->db_host = $db_host;

    }

    private function getPDO(){
        if($this->pdo === null){
            $pdo = new PDO('mysql:dbname=flemmework;host=localhost','root','root');
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo = $pdo;
        }
        return $this->pdo;
    }

    public function query($statement, $class = null, $one = false){
        $query = $this->getPDO()->query($statement);
        if(
            strpos($statement, 'UPDATE') === 0 ||
            strpos($statement, 'INSERT') === 0 ||
            strpos($statement, 'DELETE') === 0
        ){
            return $query;
        }
        if($class === null){
            $query->setFetchMode(PDO::FETCH_OBJ);
        }else{
            $query->setFetchMode(PDO::FETCH_CLASS, $class);
        }
        if($one){
            $data = $query->fetch();
        }else{
            $data = $query->fetchAll();
        }
        return $data;
    }

    public function prepare($statement, $attr, $class = null, $one = false){
        $query = $this->getPDO()->prepare($statement);
        $res = $query->execute($attr);
        if(
            strpos($statement, 'UPDATE') === 0 ||
            strpos($statement, 'INSERT') === 0 ||
            strpos($statement, 'DELETE') === 0
        ){
            return $res;
        }
        if($class === null){
            $query->setFetchMode(PDO::FETCH_OBJ);
        }else{
            $query->setFetchMode(PDO::FETCH_CLASS, $class);
        }
        if($one){
            $data = $query->fetch();
        }else{
            $data = $query->fetchAll();
        }
        return $data;
    }

    public function lastInsertId(){
        return $this->getPDO()->lastInsertId();
    }
}