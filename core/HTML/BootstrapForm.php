<?php
namespace Core\HTML;
class BootstrapForm extends Form{

    /**
     * Surround html code
     * @param $html string html tag function
     * @return string
     */
    protected function surround($html){
        return "<div class=\"form-group\">{$html}</{$this->surround}></div>";
    }

    /**
     * Create Bootstrap input form
     * @param $name
     * @param $label
     * @param array $options
     * @return string
     */
    public function input($name, $label, $options = []){
        $type = isset($options['type']) ? $options['type'] : 'text';
        $label = '<label>' . $label .'</label>';
        if($type === 'textarea'){
            $input = '<textarea name ="' .$name. '" class="form-control">'. $this->getValue($name) .'</textarea>';
        }else {
            $input = '<input type= '. $type .'  name ="' .$name. '" value="' . $this->getValue($name) .'" class="form-control">';
        }

        return $this->surround($label . $input);
    }

    public function select($name, $label, $options){
        $label = '<label>' . $label .'</label>';
        $input = '<select class="form-control" name="' .$name . '">';
        foreach ($options as $k => $v){
            $attr = '';
            if($k == $this->getValue($name)){
                $attr = ' selected';
            }
            $input .= "<option value ='$k'.$attr>$v</option>";
        }
        $input .= '</select>';
        return $this->surround($label . $input);
    }

    /**
     * Create a Bootstrap button form
     * @return string generate submit button
     */
    public function submit(){
        return  $this->surround('<button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Envoyer</button>');
    }
    
}